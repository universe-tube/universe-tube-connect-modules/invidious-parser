invidious-parser
================

Invidious-parser is the library for using invidious api. **Not ready for
using in applications.** **WARNING: TESTED ONLY ON Python3.10**

Author
======

Kostiantyn Klochko (c) 2022-2023

Donation
========

Monero:
8BPT7PzYeYqLh3iavVeisT8J2yBBfEzBsKoF6j61tqNyZQ5tN81gXLyKLrjUC8eKeaEyk5npkyYRuTZkhXW2uVwe9tDbaPZ
|image1|

License
=======

Under GNU GPL v3 license

.. |image1| image:: ./img/monero.png
