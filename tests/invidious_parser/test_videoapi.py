import pytest
from src.invidious_parser.invidious_parser import Invidious
from src.invidious_parser.invidious_config import InvidiousConfig

def test_json(invidious_preparation):
    id = "CUkuyW6hr18"
    type = "video"
    title = "5 Org Roam Hacks for Better Productivity in Emacs"

    i = invidious_preparation()
    j = i.video().get_json(id)

    assert j["type"] == type
    assert j["videoId"] == id
    assert j["title"] == title

def test_title_id(invidious_preparation):
    id = "NS1B-oebtf4"
    i = invidious_preparation()
    video_title = i.video().get_title(id)
    title = "Що таке Mastodon. Децентралізовані соцмережі"
    assert title == video_title

def test_author_id(invidious_preparation):
    id = "NS1B-oebtf4"
    i = invidious_preparation()
    id = i.video().get_author_id(id)
    author_id = "UCq6zn2sHMneKDVOHdFprf-w"
    assert author_id == id

def test_thumbnails(invidious_preparation):
    id = "NS1B-oebtf4"

    i = invidious_preparation()
    j = i.video().get_thumbnails(id)
    assert dict.pop(j, "videoId", None) == id
    assert len(dict.pop(j, "videoThumbnails", None)) == 9
    assert j == {}

def test_thumbnails(invidious_preparation):
    id = "NS1B-oebtf4"

    i = invidious_preparation()
    j = i.video().get_recommendations(id)
    assert dict.pop(j, "videoId", None) == id
    assert len(dict.pop(j, "recommendedVideos", None)) != 0
    assert j == {}
