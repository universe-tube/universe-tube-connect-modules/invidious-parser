import pytest
from src.invidious_parser.invidious_parser import Invidious
from src.invidious_parser.invidious_config import InvidiousConfig

def test_json(invidious_preparation):
    id = "UCq6zn2sHMneKDVOHdFprf-w"
    author_name = "Tokar.ua"

    i = invidious_preparation()
    j = i.channel().get_json(id)

    assert j["authorId"] == id
    assert j["author"] == author_name

def test_title(invidious_preparation):
    id = "UCq6zn2sHMneKDVOHdFprf-w"
    title = "Tokar.ua"

    i = invidious_preparation()
    channel_title = i.channel().get_title(id)

    assert title == channel_title

def test_author_id(invidious_preparation):
    channel_id = "UCq6zn2sHMneKDVOHdFprf-w"

    i = invidious_preparation()
    id = i.channel().get_author_id(channel_id)

    assert id == channel_id

def test_thumbnails(invidious_preparation):
    id = "UCq6zn2sHMneKDVOHdFprf-w"

    i = invidious_preparation()
    j = i.channel().get_thumbnails(id)

    assert dict.pop(j, "authorId", None) == id
    assert len(dict.pop(j, "authorThumbnails", None)) == 6
    assert j == {}

def test_videos(invidious_preparation):
    id = "UCq6zn2sHMneKDVOHdFprf-w"
    i = invidious_preparation()

    videos = i.channel().get_videos(id)
    assert len(videos) != 0
