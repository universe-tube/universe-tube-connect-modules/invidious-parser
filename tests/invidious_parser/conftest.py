import pytest
from src.invidious_parser.invidious_parser import Invidious
from src.invidious_parser.invidious_config import InvidiousConfig

@pytest.fixture
def invidious_preparation():
    def _method():
        ic = InvidiousConfig()
        return Invidious(ic)
    return _method
