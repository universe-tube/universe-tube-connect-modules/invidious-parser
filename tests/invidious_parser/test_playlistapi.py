import pytest
from src.invidious_parser.invidious_parser import Invidious
from src.invidious_parser.invidious_config import InvidiousConfig

def test_json(invidious_preparation):
    id = "PLnc9Jn8vKZ52YnxZScx0iZyhoQo98sMVV"
    title = "Озвучення в кадрі"

    i = invidious_preparation()
    j = i.playlist().get_json(id)
    print(j)
    assert j["playlistId"] == id
    assert j["title"] == title

def test_title(invidious_preparation):
    id = "PL9VeS5GITcUA-gsGK1Vpq-p0RZEVQRJOa"
    title = "Технології"

    i = invidious_preparation()
    playlist_title = i.playlist().get_title(id)
    assert playlist_title == title

def test_video_count(invidious_preparation):
    id = "PL9VeS5GITcUA-gsGK1Vpq-p0RZEVQRJOa"

    i = invidious_preparation()
    video_count = i.playlist().get_video_count(id)
    assert video_count != 0

def test_videos(invidious_preparation):
    id = "PL9VeS5GITcUA-gsGK1Vpq-p0RZEVQRJOa"

    i = invidious_preparation()
    videos = i.playlist().get_videos(id)
    assert len(videos) != 0
